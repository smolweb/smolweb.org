#!/bin/bash

cd "$(dirname "$0")"
export docroot="../htdocs"
export urlroot="https://smolweb.org"

mkdir -p $docroot/doc
mkdir -p $docroot/img
mkdir -p $docroot/posts
mkdir -p $docroot/specs

cp -vu *.{ico,png,css} $docroot
cp -vu img/* $docroot/img/
cp -vu doc/* $docroot/doc/


echo "# News" > news.md
echo '<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
	xmlns:thr="http://purl.org/syndication/thread/1.0"
	xml:lang="en">
	<title type="text">smolweb.org</title>
	<subtitle type="text">Smolweb news</subtitle>
	<updated>'"$(date -u -Isecond)"'</updated>
	<link rel="alternate" type="text/html" href="'"$urlroot"'/" />
	<id>'"$urlroot"'/</id>
	<link rel="self" type="application/atom+xml" href="'"$urlroot"'/atom.xml" />' > $docroot/atom.xml
# posts
for f in `ls -1r posts/*.md`;
do
	head -n 3 "$f" | grep "^# " | sed -e "s/^# //" > /tmp/title.tmp
	cat "$f" |  markdown -f fencedcode > /tmp/content.tmp
	cat "$f" |  markdown -f fencedcode -f cdata > /tmp/cdata.tmp
	cat page.tpl | sed -e "/PAGE_TITLE/r /tmp/title.tmp" -e '//d' -e "/PAGE_CONTENT/r /tmp/content.tmp" -e '//d' > $docroot/${f%.*}.html
	echo "* $(echo $f | grep -oP "[0-9]{4}-[0-9]{2}-[0-9]{2}") [$(cat /tmp/title.tmp)](${f%.*}.html)" >> news.md
	
	echo '	<entry xml:lang="en">
		<author><name>Adële</name><uri>'"$urlroot"'/</uri></author>
		<title type="html">'"$(cat /tmp/title.tmp)"'</title>
		<link rel="alternate" type="text/html" href="'"$urlroot"'/'"${f%.*}.html"'" />
		<id>'"$urlroot"'/'"${f%.*}.html"'</id>
		<published>'"$(echo $f | grep -oP "[0-9]{4}-[0-9]{2}-[0-9]{2}")"'T00:00:00+00:00</published>
		<updated>'"$(date -u -Isecond --date=$(stat -c '@%Y' $f))"'</updated>
		<content type="html">' >> $docroot/atom.xml
	cat /tmp/cdata.tmp >> $docroot/atom.xml
	echo '		</content>
	</entry>'>> $docroot/atom.xml
done
echo '</feed>' >> $docroot/atom.xml

# specs
for f in `ls -1r specs/*.md`;
do
	head -n 3 "$f" | grep "^# " | sed -e "s/^# //" > /tmp/title.tmp
	cat "$f" | sed -e "/GLOBALATTR/r specs/globalattr.md" -e '//d' |  markdown -f fencedcode > /tmp/content.tmp
	cat page.tpl | sed -e "/PAGE_TITLE/r /tmp/title.tmp" -e '//d' -e "/PAGE_CONTENT/r /tmp/content.tmp" -e '//d' > $docroot/${f%.*}.html
done

# pages
for f in *.md
do
	head -n 3 "$f" | grep "^# " | sed -e "s/^# //" > /tmp/title.tmp
	cat "$f" |  markdown -f fencedcode > /tmp/content.tmp
	cat page.tpl | sed -e "/PAGE_TITLE/r /tmp/title.tmp" -e '//d' -e "/PAGE_CONTENT/r /tmp/content.tmp" -e '//d' > $docroot/${f%.*}.html
done

