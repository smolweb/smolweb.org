# Guidelines for a smolweb

_What to do and not to do_

*Version 0.2.0 on 2024-06-15*

## How to read this document

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
"SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL"
in this document are to be interpreted as described in 
[RFC 2119](https://datatracker.ietf.org/doc/html/rfc2119).

## Write short code

To be smol, obviously, first step is to write small code and avoid 
unnecessary things. Are you sure it is useful to redefine HTML 
components with a huge CSS framework? What does it bring to your site? 
Trying to build a site which is exactly the same on all devices and 
screens is vain and expects bloated pages. Forcing a font size is even 
a bad idea for visually impaired readers. So, try to write short HTML 
code and the minimum CSS rules.

To limit computing effort, it is recommended to avoid floating elements 
(float CSS rules), blocks should be stacked vertically for an easier 
renderering.

In all case, a smolwebsite **must be readable without any CSS and 
JavaScript code**. 
The recommendations about these points are :

* **style attribute must not be used to include CSS in your HTML pages**,
a `<style>` block in the `<head>` section should be used, or, link your
stylesheet with a `<link>` tag in your head section if it is common for 
all your web pages. If a browser does not need or manage CSS (think 
about Braille display, or console mode browser), it does not need to 
load it
* **JavaScript should be avoided**, many old browsers can't execute script. 
Moreover, this code is often the cause of the swelling of sites by
increasing computing effort. However, JavaScript is allowed only if all
scripts are embedded in pages with the `<script>` tags, or from .js files
**on the same host** linked with `<link>` tag.
External script, from other sites of CDN is forbidden. In all cases,
the site must remain usable without JavaScript

To indicate the browser that you respect these contrainsts, you should add
Content Security Policy (CSP) in the headers sent by the web server. If you
can't set them, use a `meta` tag in the `<head>` of your pages:

`<meta http-equiv="Content-Security-Policy" content="default-src 'self';">`

## Use the right tags, the right way

HTML has a large set of tags and the basic error is to use them to 
format graphically your pages instead of keeping their semantic aspect. 
Some HTML tags or attributes even have a role for accessibility. 
Instead of using a simple DIV with a specific class and its style 
definition, it's a better idea to use the semantic tag corresponding to 
its content. Recent browser and specific displays are able to take 
advantage of it. `<header>`, `<footer>`, `<main>`, `<nav>`,
`<section>`, `<article>`, `<aside>`, `<details>`, `<summary>`,
`<figure>`, `<figcaption>` and `<data>` are good tags to identify the
different parts of your page.

Even if some are recent and unknown by old browsers, they are 
recommended for clean smolweb pages. They are described in the [HTML 
subset](https://smolweb.org/subset.html) page.

## Include the necessary declaration in your header code

The first thing read by a browser is the first line on your HTML page, 
so it must help to browser to determine clearly want it will find. The 
`<!DOCTYPE>` line has this role. If you want to use tags proposed in 
the [HTML subset](https://smolweb.org/subset.html) page, you must use 
the html 5 doctype : `<!DOCTYPE html>`

The second line must be the html tag with the lang code attribute 
corresponding to the language of the page content (see the [ISO 639-1 
codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)) : 
`<html lang="en">`

Indicating which encoding (or character set) is used in your page 
permit to avoid bad display of accent. UTF-8 does everything you want 
(and more), it’s a widely accepted standard and basically supported 
everywhere: UTF-8 must be used! This declaration is a meta tag in 
the head section:    
`<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >`

And, to be compatible with all size of screen, defining the viewport to 
the width of the screen device is very simple and recommended. Just add 
another meta in the head section:     
`<meta name="viewport" content="width=device-width, initial-scale=1.0" >`

To respect the user choice between light and dark mode, if you don't
specify background and foreground colors in your stylesheet, you should 
add this simple meta in the head:    
`<meta name="color-scheme" content="light dark" type="text/css" >`

The head section can also contain links to external resources such as CSS 
stylecheets    
`<link href="/style.css" rel="stylesheet" >`

It's always a good idea to provide a favicon, because browsers are
looking for `/favicon.ico` even if this file does not exist, and they do
it for every page. Providing one allows them to cache it a the first
request. It should be proposed within different formats...    
`<link rel="icon" href="/favicon.ico" type="image/x-icon" >`    
`<link rel="icon" href="/favicon.png" type="image/png" >`

## Do not use media content if not necessary

If you embed images in your pages, try to make them small. Eventually, 
you can provide a bigger version making a link on the small img to the 
bigger one. You must provide an alternative description in the alt 
attribute of the img tag. Browsers can be set to not load images until 
the user decide to load them explicitly. Alt text help to decide to load 
or not.

Does your pages really need a logo? Sometimes, a simple text or word 
could be enough.

Limit the use of audio and video to the strict minimum. If you use 
audio or video tag, you must not set the autoplay attribute, and set 
the control attribute. The `<source`> elements permit de specify 
several format of the media. So, the user may choose to play or not 
the media, and the format according to the browser capabilities.

## Do not embed font

Loading a font from your web server or from a specific CDN will bloat 
your site. Try to stick to common fonts available on almost all 
computers with a fallback to a default `serif`, `sans-serif` or 
`monospace`. These three fonts use fonts that the user has defined as 
its preferred ones in its browser settings.

You will find a good resource on the great
[Modern Font Stacks](https://modernfontstacks.com/) compilation.

In all case, be sure that you don't use glyph in your text that are 
not defined in standard system fonts.

## Ensure readibility

Good practice, for accessibility, is to respect the font size defined by
the user.

```
html { font-size: 100%; }
```

And to adapt your style relatively to the root (`html` element) size
using rem units.

```
h1 { font-size: 2rem; }
h2 { font-size: 1.6rem; }
h3 { font-size: 1.2rem; }
small { font-size: 0.8rem; }
```

## Use emoticons, not emojis

Recent unicode characters, such as emojis, are not recognized by old 
browsers. Remember the good old time of emoticons, made with ASCII 
characters :-) if you need to specify a mood or to precise irony or 
humour, they still are a good alternative. Another option is to use 
small images (with alt text describing them shortly).

## Use table for tabular data only

At the beginning of the web, before 2000, tables were used to format a 
webpage graphically. This is a real bad solution. Use tables, only if 
necessary, and to format data (think about spreadsheets).

## Check your code

It is easy to write bad code. W3C provides a [tool to 
check](https://validator.w3.org/nu/) that your page is conforming to 
standard.

Try your site on different kind of browser such as recent browsers 
(Firefox, Chromium), console mode browsers (Lynx, W3M), old widely 
ported simple browsers (Netsurf, Dillo, Mosaic) and on small screen 
devices (smartphones and tablets). If you haven't these specific 
browsers, try your site without its stylesheet: it would be less 
beautiful (maybe) but it should remain usable.

## Supply an RSS feed

Smolweb surfers like to have the possibility to follow you without a 
specific social network. RSS feeds are always used by many internauts. 
They permit to be aware of new publication on your site. If your posts 
are short, please, put full content of your last articles in your RSS 
xml file.

## Enable https protocol

https (http over TLS on port 443) is standard since Letsencrypt 
proposes free certificates. It would be a good idea to supply simple
http (on port 80) if you want your site is usable by very old browsers
or on systems with old CA chains. However, your site could become a point
of failure for a man-in-the-middle attack. So, ensure to enable https and
a redirection from http connections to https.

## Code sample

Here is a sample of a standard HTML page using these recommendations :

```
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<meta http-equiv="Content-Security-Policy" content="default-src 'self';">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<meta name="description" content="My smolwebsite about all my hobbies..." >
		<meta name="color-scheme" content="light dark" >
		<link href="/style.css" rel="stylesheet" type="text/css" >
		<link rel="icon" href="/favicon.ico" type="image/x-icon" >
		<link rel="icon" href="/favicon.png" type="image/png" >
		<link rel="icon" href="/favicon.gif" type="image/gif" >
		<link rel="alternate" type="application/rss+xml"
			href="https://mysmolwebsite.tld/rss.xml" title="My smolwebsite!" >
		<title>The title of my page</title>
	</head>
	<body>
		<header>
		<strong>My smolwebsite</strong>
		<nav><ul>
			<li><a href="index.html">About</a></li>
			<li><a href="posts/index.html">Posts</a></li>
			<li><a href="contact.html">Contact</a></li>
		</ul></nav> 
		</header>
		<main>
			<article>
			<h1>My first post</h1>
			<p>Happy to have a smolwebsite!</p>
			</article>
			<aside>
				Other related posts:
				<ul>
					<li><a href="other-post.html">other post</a></li>
				</ul>
			</aside>
		</main>
		<footer>
			Published by <strong>me</strong> under Creative Commons BY SA
		</footer>
	</body>
</html>
```
