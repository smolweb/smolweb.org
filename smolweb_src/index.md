# About smolweb

_Promoting a simple unbloated web!_

Nowadays, hardware resources of our personal devices are essentially 
oversized to manage bloated websites. If we incite web designers to 
return to a lighter web, small devices such as old PC, old smartphones, 
retro-machines and small boards could be usable.

This is the goal of the smolweb concept.

But what is a bloated website? Two aspects are critical: the size of 
the code and linked resources downloaded by the browser, and the 
computing effort to do the layouting by transforming it into absolutely 
positioned boxes with text and images. Reducing both can make a website 
"smol"

This site attempts to define what a smolweb is and to give some 
[guidelines](https://smolweb.org/guidelines.html) for use. It is 
certainly not the only solution to get unbloated websites, but it will 
help authors, designers and webmasters who want to build 
"smolwebsites". Do not read these recommendations as a bible, this is 
only what I think to be a good way to build respectful websites for 
authors, readers, internet bandwidth and hardware.
