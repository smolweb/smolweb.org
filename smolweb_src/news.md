# News
* 2024-07-22 [Simple.css: a minimalist css framework](posts/2024-07-22-simple-css.html)
* 2024-06-15 [Allowing JavaScript in smolweb](posts/2024-06-15-allowing-javascript-in-smolweb.html)
* 2023-12-23 [Adding figure and figcaption tags](posts/2023-12-23-adding-figure-tags.html)
* 2023-10-16 [Wiby, a search engine for the smolweb](posts/2023-10-16-wiby-a-search-engine-for-the-smolweb.html)
* 2023-10-11 [A smolweb bash generator](posts/2023-10-11-smolweb-bash-generator.html)
* 2023-10-09 [An other Small Web definition](posts/2023-10-09-an-other-smallweb-definition.html)
* 2023-10-09 [First post](posts/2023-10-09-1st-post.html)
