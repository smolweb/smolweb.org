<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<meta name="description" content="smolweb.org promotes simple unbloated web. It provides resources to actors who want to participate." >
		<link href="/style.css?v6" rel="stylesheet" type="text/css" >
		<link href="/atom.xml" rel="alternate" type="application/atom+xml" title="Smolweb news" >
		<link rel="icon" href="/favicon.ico" type="image/x-icon" >
		<link rel="icon" href="/favicon-64x64.png" type="image/png" >
		<title>
			PAGE_TITLE
		</title>
	</head>
	<body>
		<header>
		<p>
			<small><code>(w)</code></small> smolweb
		</p>
		<nav><ul>
			<li><a href="/index.html">Home</a></li>
			<li><a href="/guidelines.html">Guidelines</a></li>
			<li><a href="/specs/index.html">HTML subset</a></li>
            <li><a href="/resources.html">Resources</a></li>
			<li><a href="/news.html">News</a></li>
			<li><a href="/atom.xml">RSS feed</a></li>
		</ul></nav> 
		</header>
		<main>
			<article>
				PAGE_CONTENT
			</article>
		</main>
		<footer>
			<p>
				Portions of HTML subset pages are by individual mozilla.org contributors under a <a href="https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines/Attrib_copyright_license">Creative Commons license</a>.<br>
				Other <a property="dct:title" rel="cc:attributionURL" href="https://smolweb.org/">Smolweb</a> content by <a rel="cc:attributionURL dct:creator me" property="cc:attributionName" href="https://phpc.social/@adele">Adële</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="license noopener noreferrer">CC BY-SA 4.0</a><br>
				Want to contribute to smolweb.org? see the <a href="https://codeberg.org/smolweb/smolweb.org">source</a> and <a href="https://codeberg.org/smolweb/smolweb.org/issues">issue tracker</a> on Codeberg.
			</p> 
		</footer>
	</body>
</html>
