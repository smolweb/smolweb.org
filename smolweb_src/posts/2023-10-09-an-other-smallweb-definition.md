# An other Small Web definition

_2023-10-09_

As mentioned on the [homepage of smolweb.org](https://smolweb.org/index.html), this site gives one definition and some guidelines to build a better and unbloated web.

But there are many solutions to reach this goal. Aral Balkan give a different [definition of the Small Web](https://ar.al/2020/08/07/what-is-the-small-web/), and it is interesting have a look.

In addition, he proposes several tools, such as [Kitten](https://codeberg.org/kitten/app) to build this new web.

You can follow him on [Mastodon](https://mastodon.ar.al/@aral).
