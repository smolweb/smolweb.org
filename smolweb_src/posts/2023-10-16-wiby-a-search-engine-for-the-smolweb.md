# Wiby, a search engine for the smolweb

While looking for smolweb resources, I met [Wiby](https://wiby.me)! This search engine indexes websites compatible with old browsers. You will find simple HTML and non-commercial sites, and, if you have a smolwebsite, you can [submit it to their database](https://wiby.me/submit/).

It also works with [http protocol](http://wiby.me/).

A very good resource for smolweb surfers!
