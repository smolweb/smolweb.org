# Adding figure and figcaption tags

_2023-12-23_

As suggested by [@reiver on the Fediverse](https://mastodon.social/@reiver/111613323404260453), the semantic tags `<figure>` and `<figcaption>` have been added to the smolweb subset. 

