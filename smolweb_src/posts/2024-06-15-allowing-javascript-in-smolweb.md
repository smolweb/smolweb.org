# Allowing JavaScript in smolweb

_2024-06-15_

Following [the poll on the Fediverse](https://phpc.social/@adele/112584960486034900) and the discussion under it, JavaScript could be allowed on smolweb sites under some conditions :

- The site must remain usable without JavaScript, if the browser has no JavaScript engine or if the user disable it ;
- All scripts must be embedded directly in the pages of the smolweb site or in external .js files coming from the `same host`.