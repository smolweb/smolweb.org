# Simple.css: a minimalist css framework

_2024-07-22_

If building a stylesheet in css from scratch seems too complicated for you, [Simple.css](https://simplecss.org/) is a solution. This lightweight framework stands on HTML elements (and their semantic specs) and not on specific classes. So, your pages can fully follow the [smolweb guidelines](/guidelines.html) and get a responsive design that respect the choices of the user such as font sizes and light/dark preference.
