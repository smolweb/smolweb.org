# Resources

## Search engines

* [Wiby](https://wiby.me/) : search engine for small websites
* [Kagi's smallweb](https://kagi.com/smallweb/) : Kagi search engine, dedicated page (random site)

## Other Smol/Small Web definitions

* [Aral's def](https://ar.al/2020/08/07/what-is-the-small-web/) : "What is the Small Web?" by Aral Balkan
* [Ben's def](https://benhoyt.com/writings/the-small-web-is-beautiful/) : "The small web is beautiful" by Ben Hoyt
* [Terence's def](https://shkspr.mobi/blog/2021/01/the-unreasonable-effectiveness-of-simple-html/) : "The unreasonable effectiveness of simple HTML" by Terence Eden

## Tools

* [Smol theme](https://themes.gohugo.io/themes/smol/) for Hugo static site generator

## Other resources

* [Web Sustainability Guidelines](https://w3c.github.io/sustainableweb-wsg/) : a W3C's draft covering a wide range of recommendations for making websites and products more sustainable. See also its [working repo](https://github.com/w3c/sustainableweb-ig/)
* [Awsome-small-web-publishing](https://codeberg.org/thgie/awesome-small-web-publishing) : a curated list of awesome small web publishing tools and frameworks
* [ESIF](https://esif.dev) : a historical record of foundational web development blog posts
* [smallweb.page](https://smallweb.page/home) : introduction to the movement that's called the "small web"
* [HTML Hobbyist](https://htmlhobbyist.neocities.org/) : for people who want to make websites
* [HTML for People](https://htmlforpeople.com/) : resource designed to make HTML simpler and more intuitive, especially for beginners
