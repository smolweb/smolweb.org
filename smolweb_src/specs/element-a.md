# a: The Anchor element

The `<a>` HTML element (or anchor element), with its [`href`](attr-href.md) attribute, creates a hyperlink to web pages, files, email addresses, locations in the same page, or anything else a URL can address.

Content within each `<a>` should indicate the link's destination. If the href attribute is present, pressing the enter key while focused on the `<a>` element will activate it.

## Attributes

### Specific attributes

[`role`](attr-role.html)`="link"` when href attribute is present, otherwise `generic`

`download="[filename]"` causes the browser to treat the linked URL as a download. Can be used with or without a filename value. The filename will be proposed to save the file locally.

`href="[URL]"` the URL that the hyperlink points to. Links are not restricted to HTTP-based URLs — they can use any URL scheme supported by browsers:

* Sections of a page with document fragments
* Specific text portions with text fragments
* Pieces of media files with media fragments
* Telephone numbers with `tel:` URLs
* Email addresses with `mailto:` URLs
* SMS text messages with `sms:` URLs

`hreflang="[lang]"` hints at the human language of the linked URL. Allowed values are defined by [RFC 5646](https://datatracker.ietf.org/doc/html/rfc5646)

`referrerpolicy="[type of policy]"` defines part of the referrer to send when following the link.

* no-referrer: The Referer header will not be sent.
* no-referrer-when-downgrade: The Referer header will not be sent to origins without TLS (HTTPS).
* origin: The sent referrer will be limited to the origin of the referring page: its scheme, host, and port.
* origin-when-cross-origin: The referrer sent to other origins will be limited to the scheme, the host, and the port. Navigations on the same origin will still include the path.
* same-origin: A referrer will be sent for same origin, but cross-origin requests will contain no referrer information.
* strict-origin: Only send the origin of the document as the referrer when the protocol security level stays the same (HTTPS→HTTPS), but don't send it to a less secure destination (HTTPS→HTTP).
* strict-origin-when-cross-origin (default): Send a full URL when performing a same-origin request, only send the origin when the protocol security level stays the same (HTTPS→HTTPS), and send no header to a less secure destination (HTTPS→HTTP).
* unsafe-url: The referrer will include the origin and the path (but not the fragment, password, or username). This value is unsafe, because it leaks origins and paths from TLS-protected resources to insecure origins.

`target="[target value]"` Where to display the linked URL, as the name for a browsing context (a tab, window). The following keywords have special meanings for where to load the URL:

* _self: The current browsing context. (Default)
* _blank: Usually a new tab, but users can configure browsers to open a new window instead.
* _parent: The parent browsing context of the current one. If no parent, behaves as _self.
* _top: The topmost browsing context. To be specific, this means the "highest" context that's an ancestor of the current one. If no ancestors, behaves as _self.
* _unfencedTop: Allows embedded fenced frames to navigate the top-level frame (i.e. traversing beyond the root of the fenced frame, unlike other reserved destinations). Note that the navigation will still succeed if this is used outside of a fenced frame context, but it will not act like a reserved keyword.

`type="[mime type]" Hints at the linked URL's format with a MIME type.

GLOBALATTR


## Example

```
<a href="https://smolweb.org" target="_blank" referrerpolicy="origin">SmolWeb</a>

```
