# abbr: The Abbreviation element

When including an abbreviation or acronym, provide a full expansion of the term in plain text on first use, along with the `<abbr>` to mark up the abbreviation. This informs the user what the abbreviation or acronym means.

The optional title attribute can provide an expansion for the abbreviation or acronym when a full expansion is not present. This provides a hint to user agents on how to announce/display the content while informing all users what the abbreviation means. If present, title must contain this full description and nothing else.

## Attributes

### Specific attributes

`title="[meaning of abbreviation]"` The title attribute has a specific semantic meaning when used with the `<abbr>` element; it must contain a full human-readable description or expansion of the abbreviation. This text is often presented by browsers as a tooltip when the mouse cursor is hovered over the element.

GLOBALATTR

## Example

```
<p>I like <abbr title="HyperText Markup Language">HTML</abbr>!</p>

```