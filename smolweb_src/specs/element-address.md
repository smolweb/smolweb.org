# address: The Contact Address element

The `<address>` HTML element indicates that the enclosed HTML provides contact information for a person or people, or for an organization.

## Attributes

[`role`](attr-role.html)`="group"` is the implicit value

GLOBALATTR

## Example

```
<address>
  You can contact me at
  <a href="https://mysite.tld/contact"mysite.tld/contact/a>.<br>
  or send an email to
  <a href="mailto:me@mysite.tld">me@mysite.tld</a>.<br>
  You may also want to visit our office:<br>
  My Office<br>
  Rue de la Libération<br>
  92210 Saint-Cloud<br>
  FRANCE
</address>

```