# article: The Article Contents element

The `<article>` HTML element represents a self-contained composition in a document, page, application, or site, which is intended to be independently distributable or reusable (e.g., in syndication). Examples include: a forum post, a magazine or newspaper article, or a blog entry, a product card, a user-submitted comment, an interactive widget or gadget, or any other independent item of content.

A given document can have multiple articles in it; for example, on a blog that shows the text of each article one after another as the reader scrolls, each post would be contained in an `<article>` element, possibly with one or more `<section>`s within.

## Attributes

[`role`](attr-role.html)`="article"` is the implicit value

GLOBALATTR

## Example

```
<main role="feed">
  <h1>Lasts articles</h1>
  <article>
    <h2>Climate is warming</h2>
    <header>
      <p>posted on <time datetime="2024-04-09">Apr. 9</time></p>
    </header>
    <p>How can we stop it...
       ....</p>
  </article>
  <article>
    <h2>Web is bloated</h2>
    <header>
      <p>posted on <time datetime="2024-04-07">Apr. 7</time></p>
    </header>
    <p>How can we unbloat it...
       ....</p>
  </article>
</main>
```