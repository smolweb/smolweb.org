# aside: The Aside element

The `<aside>` HTML element represents a portion of a document whose content is only indirectly related to the document's main content. Asides are frequently presented as sidebars or call-out boxes.

## Attributes

[`role`](attr-role.html)`="complementary"` is the implicit value. You should specify which is the [`role`](attr-role.html) of the aside according to its content.

GLOBALATTR

## Example

```
<main>
  <article>
    <h1>Climate is warming</h1>
    <header>
      <p>posted on <time datetime="2024-04-09">Apr. 9</time></p>
    </header>
    <p>How can we stop it...
       ....</p>
    <aside role="note">
       <p>That's not new, we know it for several decades...</p>
    </aside>
  </article>
</main>
```