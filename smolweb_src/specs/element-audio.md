# audio: The Embed Audio element

The `<audio>` HTML element is used to embed sound content in documents. It may contain one or more audio sources, represented using the src attribute or the [`<source>`](element-source.html) element: the browser will choose the most suitable one. It can also be the destination for streamed media, using a MediaStream.

## Attributes

### Specific attributes

You must not use the `autoplay` attribute.

`controls` if this attribute is present, the browser will offer controls to allow the user to control audio playback, including volume, seeking, and pause/resume playback.
controlslist

`controlslist="[type of controls]` when specified, helps the browser select what controls to show for the audio element whenever the browser shows its own set of controls (that is, when the controls attribute is specified). The allowed values are `nodownload`, `nofullscreen` and `noremoteplayback`.

`loop` if specified, the audio player will automatically seek back to the start upon reaching the end of the audio.

`muted` indicates whether the audio will be initially silenced. Its default value is false.

`preload="[type of preload]` is intended to provide a hint to the browser about what the author thinks will lead to the best user experience. It may have one of the following values:

* none: Indicates that the audio should not be preloaded.
* metadata: Indicates that only audio metadata (e.g. length) is fetched.
* auto: Indicates that the whole audio file can be downloaded, even if the user is not expected to use it. This option is not recommended!
* empty string: A synonym of the auto value.

`src="[URL]"` specifies the URL of the audio to embed. This is subject to HTTP access controls. This is optional; you may instead use the [`<source>`](element-source.html) element within the audio block to specify the audio to embed in one or multiple formats/qualities.

GLOBALATTR

## Example

```
<audio controls>
  <source src="media/myAudio.mp3" type="audio/mpeg">
  <source src="media/myAudio.ogg" type="audio/ogg">
  <p>
    Download <a href="media/myAudio.mp3" download="myAudio.mp3">MP3</a> or
    <a href="media/myAudio.ogg" download="myAudio.ogg">OGG</a> audio file.
  </p>
</audio>

```