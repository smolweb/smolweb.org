# b: The Bring Attention To element

The `<b>` HTML element is used to draw the reader's attention to the element's contents, which are not otherwise granted special importance. This was formerly known as the Boldface element, and most browsers still draw the text in boldface. However, you should not use `<b>` for styling text or granting importance. If you wish to create boldface text, you should use the CSS font-weight property. If you wish to indicate an element is of special importance, you should use the `<strong>` element.

## Attributes

No specific attribute

GLOBALATTR

## Example

```
<p>It is important to <b>reduce CSS code</b> to get a smolweb site.</p>

```