# base: The Document Base URL element

The `<base>` HTML element specifies the base URL to use for all relative URLs in a document. There can be only one `<base>` element in a document. It is a child of the [`head`](element-head.html) element.

## Attributes

### Specific attributes

`href` is the base URL to be used throughout the document for relative URLs. Absolute and relative URLs are allowed. data: and javascript: URLs are not allowed.

`target` contains a keyword or author-defined name of the default browsing context to show the results of navigation from [`a`](element-a.html) or [`form`](element-form.html) elements without explicit target attributes. The following keywords have special meanings:

* _self (default): Show the result in the current browsing context.
* _blank: Show the result in a new, unnamed browsing context.
* _parent: Show the result in the parent browsing context of the current one, if the current page is inside a frame. If there is no parent, acts the same as _self.
* _top: Show the result in the topmost browsing context (the browsing context that is an ancestor of the current one and has no parent). If there is no parent, acts the same as _self.

GLOBALATTR

## Example

```
<base target="_top" href="https://example.com/" />
```
