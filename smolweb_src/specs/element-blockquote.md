# blockquote: The Block Quotation element

The `blockquote` HTML element indicates that the enclosed text is an extended quotation. Usually, this is rendered visually by indentation. A URL for the source of the quotation may be given using the cite attribute, while a text representation of the source can be given using the [`cite`](element-cite.html) element.

## Attributes

### Specific attributes

`cite` associates the URL that designates a source document or message for the information quoted. This attribute is intended to point to information explaining the context or the reference for the quote.

GLOBALATTR

## Example

```
<blockquote cite="https://www.w3.org/standards/webdesign/accessibility">
  <p>The power of the Web is in its universality. Access by everyone regardless of disability is an essential aspect</p>
  Tim Berners Lee, <cite>W3C about Accessibility</cite>
</blockquote>
```
