# body: The Document Body element

The `<body>` HTML element represents the content of an HTML document. There can be only one <body> element in a document.

* Permitted content: [*Flow content* elements](flowcontent.html)
* Permitted parents: The [`<html>`](element-html.html) element must be the direct parent

## Attributes

[`role`](attr-role.html)`="generic"` is the implicit value

GLOBALATTR

## Example

```
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>The title of my page</title>
	</head>
    <body>
        <h1>Hello</h1>
    </body>
</html>
```

