`br`: The Line Break element

The `br` HTML element produces a line break in text (carriage-return). It is useful for writing a poem or an address, where the division of lines is significant.

## Attributes

No specific attribute

GLOBALATTR

## Example

```
<p>The <code>br</code> element permits to go<br>
at the begining of the next line.</p>

```