# cite: The Citation element

The `cite` HTML element is used to mark up the title of a cited creative work. The reference may be in an abbreviated form according to context-appropriate conventions related to citation metadata.

To include a reference to the source of quoted material which is contained within a [`blockquote`](element-blockquote.html) or [`q`](element-q.html) element, use the cite attribute on the element.

Typically, browsers style the contents of a `cite` element in italics by default.

## Attributes

No specific attribute

GLOBALATTR

## Example

```
<blockquote cite="https://www.w3.org/standards/webdesign/accessibility">
  <p>The power of the Web is in its universality. Access by everyone regardless of disability is an essential aspect</p>
  Tim Berners Lee, <cite>W3C about Accessibility</cite>
</blockquote>
```
