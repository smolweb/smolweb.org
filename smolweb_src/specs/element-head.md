# head: The Document Metadata (Header) element

The `<head>` HTML element contains machine-readable information 
(metadata) about the document, like its title and style sheets.

* Permitted content: One [`<title>`](element-title.html), one [`<base>`](element-base.html), some [`<meta>`](element-meta.html) and [`<link>`](element-link.html) elements
* Permitted parents: The [`<html>`](element-html.html) element must be the direct parent

## Attributes

No specific attribute

GLOBALATTR

## Example

```
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<meta name="description" content="My smolwebsite about all my hobbies..." >
		<link href="/style.css" rel="stylesheet" type="text/css" >
		<link rel="icon" href="/favicon.ico" type="image/x-icon" >
		<link rel="alternate" type="application/rss+xml"
			href="https://mysmolwebsite.tld/rss.xml" title="My smolwebsite!" >
		<title>The title of my page</title>
	</head>
	...
```
