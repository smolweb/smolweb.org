# html: The HTML Document / Root element

The `<html>` HTML element represents the root (top-level element) of an 
HTML document, so it is also referred to as the root element. All other 
elements must be descendants of this element.

* Permitted content: One [`<head>`](element-head.html) element, followed by one [`<body>`](element-body.html) element. 
* Permitted parents: None. This is the root element of a document.

## Attributes

### Specific attributes

[`role`](attr-role.html)`="document"` is the implicit value

[`lang`](attr-lang.html)`="[language-code]"` must define the main language of the document as defined by [RFC 5646](https://datatracker.ietf.org/doc/html/rfc5646)

GLOBALATTR
