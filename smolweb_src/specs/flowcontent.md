# Flow content elements

Flow content is a broad category that encompasses most elements that can go inside the [`<body>`](element-body.html) element, including heading elements, sectioning elements, phrasing elements, embedding elements, interactive elements, and form-related elements. It also includes text nodes (but not those that only consist of white space characters).

## Different subset type belong to the flow content

### Semantic subset

Identify the different sections or area in the body of the document

* [`<header>`](element-header.html) an introductory content
* [`<footer>`](element-footer.html) a footer for its nearest ancestor sectioning content
* [`<main>`](element-main.html) the dominant content of the document
* [`<nav>`](element-nav.html) a section of a page whose purpose is to provide navigation links
* [`<section>`](element-section.html) a generic standalone section of the document
* [`<article>`](element-article.html) a self-contained composition in the document (distributable or reusable)
* [`<aside>`](element-aside.html) a portion of a document which is indirectly related to the document's main content
* [`<details>`](element-details.html) a disclosure widget in which information is not visible directly
* [`<summary>`](element-summary.html) inside a [`<details>`](element-details.html) element, summarize its content
* [`<figure>`](element-figure.html) a self-contained content, potentially with an optional caption
* [`<figcaption>`](element-figcaption.html) a caption or legend describing the rest of the contents of its parent [`<figure>`](element-figure.html) element
* [`<data>`](element-data.html) a given piece of content with a machine-readable translation

### Textual subset

Defines the level or the type of content in the document.

* [`<abbr>`](element-abbr.html) an abbreviation or acronym
* [`<address>`](element-address.html) contact information for a person or people, or for an organization
* [`<blockquote>`](element-blockquote.html) an extended quotation
* [`<br>`](element-br.html) a line break in text
* [`<cite>`](element-cite.html) mark up the title of a cited creative work
* [`<code>`](element-code.html) a short fragment of computer code
* [`<dfn>`](element-dfn.html) the term being defined within the context of a definition phrase or sentence
* [`<div>`](element-div.html) a generic container for flow content
* [`<em>`](element-em.html) text having stress emphasis
* [`<h1>`](element-h1.html) the higher level of section headings
* [`<h2>`](element-h2.html) second level of section headings
* [`<h3>`](element-h3.html) third level of section headings
* [`<h4>`](element-h4.html) forth level of section headings
* [`<h5>`](element-h5.html) fifth level of section headings
* [`<h6>`](element-h6.html) the lower level of section headings
* [`<kbd>`](element-kbd.html) inline text denoting textual user input from a keyboard
* [`<p>`](element-p.html) a paragraph
* [`<pre>`](element-pre.html) a preformatted text which is to be presented exactly as written
* [`<q>`](element-q.html) a short inline quotation
* [`<samp>`](element-samp.html) inline sample (or quoted) output from a computer program
* [`<span>`](element-span.html) a generic inline container for phrasing content
* [`<strong>`](element-strong.html) content having strong importance, seriousness, or urgency
* [`<var>`](element-var.html) the name of a variable in a mathematical expression or a programming context

### Hypertextual subset

Link with other web resources.

* [`<a>`](element-a.html) a hyperlink to web pages, files, email addresses, locations... or a link's destination

### Listing subset

List representation of the content such as bulleted or ordered lists

* [`<dl>`](element-dl.html) a description list
* [`<dt>`](element-dt.html) a term in a description or definition list [`<dl>`](element-dl.html) 
* [`<dd>`](element-dd.html) the description, definition, or value for the preceding [`<dt>`](element-dt.html)
* [`<ol>`](element-ol.html) an ordered list of items
* [`<ul>`](element-ul.html) an unordered list of items (bulleted list)
* [`<li>`](element-li.html) an item in an [`<ol>`](element-ol.html) or [`<ul>`](element-ul.html) list

### Forms subset

Allow the user to input or modify data fields in the document and to submit them to the server.

* [`<button>`](element-button.html) an interactive element activated by a user, performing an action
* [`<fieldset>`](element-fieldset.html) a group of several controls in a [`<form>`](element-form.html)
* [`<form>`](element-form.html) a document section containing interactive controls for submitting information
* [`<input>`](element-input.html) interactive controls accepting data from the user
* [`<label>`](element-label.html) a caption for an item
* [`<legend>`](element-legend.html) a caption for the content of its parent [`<fieldset>`](element-fieldset.html)
* [`<select>`](element-select.html) a control that provides a menu of options
* [`<optgroup>`](element-optgroup.html) a grouping of options within a [`<select>`](element-select.html)
* [`<option>`](element-option.html) an item contained in a [`<select>`](element-select.html) or an [`<optgroup>`](element-optgroup.html)
* [`<textarea>`](element-textarea.html) a multi-line plain-text editing control

### Basic Table subset

Represent tabular data (think about spreadsheets notion).

* [`<caption>`](element-caption.html) the caption (or title) of a [`<table>`](element-table.html) 
* [`<table>`](element-table.html) two-dimensional tabular data
* [`<td>`](element-td.html) a cell of a [`<table>`](element-table.html) that contains data
* [`<th>`](element-th.html) a cell as the header of a group of [`<table>`](element-table.html) cells
* [`<tr>`](element-tr.html) a row of cells in a [`<table>`](element-table.html)

### Image subset

* [`<img>`](element-img.html) an element embedding an image into the document

### Presentation subset

Historically used for styling the content, these elements have, now, a more semantic role.

* [`<b>`](element-b.html) content requiring the reader's attention 
* [`<hr>`](element-hr.html) a thematic break between paragraph-level elements
* [`<i>`](element-i.html) text that is set off from the normal text for some reason
* [`<small>`](element-small.html) side-comments, small print
* [`<sub>`](element-sub.html) text which should be displayed as subscript for solely typographical reasons
* [`<sup>`](element-sup.html) text which is to be displayed as superscript for solely typographical reasons


