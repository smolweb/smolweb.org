
### Global attributes

[`role`](attr-role.md)`="[semantic-role]"`

[`accesskey`](attr-accesskey.md)`="[key]"` 

[`autofocus`](attr-autofocus.md)

[`class`](attr-class.md)`="[classname]"`

[`dir`](attr-dir.md)`="[auto/ltr/rtl]"`

[`hidden`](attr-hidden.md)

[`id`](attr-id.md)`="[identifier]"`

[`itemid`](attr-itemid.md)`="[global-identifier]"`

[`itemprop`](attr-itemprop.md)`="[property-name]"`

[`itemscope`](attr-itemscope.md) [`itemtype`](attr-itemtype.md)`="[url of the vocabulary]"`

[`lang`](attr-lang.md)`="[language-code]"`

[`tabindex`](attr-tabindex.md)`="[num index]"`

[`title`](attr-title.md)`="[Title]"`
