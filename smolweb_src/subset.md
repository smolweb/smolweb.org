# HTML subset for smolweb
This subset is inspired by a previous work proposed by W3C: the [XHTML Basic](https://www.w3.org/TR/xhtml-basic/)

The XHTML Basic document type includes the minimal set of modules required to be an XHTML host language document type, and in addition it includes images, forms, basic tables, and object support. It is designed for Web clients that do not support the full set of XHTML features; for example, Web clients such as mobile phones, PDAs, pagers, and set top boxes. The document type is rich enough for content authoring.

For a better compatibility, it is not a good idea to specify the XHTML Basic 1.1 DTD in the doctype for smolwebsites.

Some deprecated tags (accronym, big, tt) have been removed from this list. Object and param tags have been banned to avoid inclusion of specific code such as Java applets.

As specify in Guidelines, semantics tags issued in more recent HTML versions have been added to propose a better accessibility.

## List of HTML elements subset

Refer to [Mozilla Developer Network documentation](https://developer.mozilla.org/en-US/docs/Web/HTML/Element) to get detailed information of each element tag listed below.

### Structural subset

* `<body>`
* `<head>`
* `<html>`
* `<title>`

### Semantic subset

* `<header>`
* `<footer>`
* `<main>`
* `<nav>`
* `<section>`
* `<article>`
* `<aside>`
* `<details>`
* `<summary>`
* `<figure>`
* `<figcaption>`
* `<data>`

### Textual subset

* `<abbr>`
* `<address>`
* `<blockquote>`
* `<br>`
* `<cite>`
* `<code>`
* `<dfn>`
* `<div>`
* `<em>`
* `<h1>`
* `<h2>`
* `<h3>`
* `<h4>`
* `<h5>`
* `<h6>`
* `<kbd>`
* `<p>`
* `<pre>`
* `<q>`
* `<samp>`
* `<span>`
* `<strong>`
* `<var>`

### Hypertextual subset

* `<a>`

### Listing subset

* `<dl>`
* `<dt>`
* `<dd>`
* `<ol>`
* `<ul>`
* `<li>`

### Forms subset

* `<button>`
* `<fieldset>`
* `<form>`
* `<input>`
* `<label>`
* `<legend>`
* `<select>`
* `<optgroup>`
* `<option>`
* `<textarea>`

### Basic Table subset

* `<caption>`
* `<table>`
* `<td>`
* `<th>`
* `<tr>`

### Image subset

* `<img>`

### Presentation subset

* `<b>`
* `<hr>`
* `<i>`
* `<small>`
* `<sub>`
* `<sup>`

### Metainformation and linking subset

* `<meta>`
* `<link>`
* `<base>`
* `<script>`
* `<style>`


_this document is always under construction!_
