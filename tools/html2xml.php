#!/usr/bin/php
<?php

$url="https://jpfox.fr/";

$html = file_get_contents($url);

// Configuration
$config = array(
           'show-info'      => true,
           'show-warnings'  => true,
           'indent'         => true,
           'output-xml'   		=> true,
           'drop-empty-elements' => false,
           'drop-empty-paras' => false,
           'drop-proprietary-attributes' => false,
           'merge-divs' => false,
           'merge-spans' => false,
           'preserve-entities' => true,
           'warn-proprietary-attributes'   => true,
           'accessibility-check'   => 1,
           'show-meta-change'=>true,
           'hide-comments'=>true,
           'merge-emphasis'=> false,
           'tidy-mark'=> false,
           'wrap'           => 200);

// Tidy

$tidy = new tidy();

$tidy->parseString($html, $config, 'utf8');
$tidy->cleanRepair();

print_r($tidy->errorBuffer);

print $tidy;

$rTagStack=array();
$currentTag = "";
$level=0;
$rResult=array();
function startElement($parser, $name, $attribs) 
{ 
	global $currentTag, $rTagStack, $level, $rResult; 
	$currentTag = $name; 

	$rTagStack[]= $name;
	$level++;
	$lineNb = sprintf('%06d', xml_get_current_line_number($parser));
	$rResult[]= $lineNb.str_repeat('  ',$level)."/$name\n";
} 

function endElement($parser, $name) 
{ 
	global $currentTag, $rTagStack, $level, $rResult; 

	$lineNb = sprintf('%06d', xml_get_current_line_number($parser));
	$rResult[]= $lineNb.str_repeat('  ',$level)." $name\n";
	if(end($rTagStack)==$name)
	{
		array_pop($rTagStack);
		$level--;
	}
	else
		$rResult[]= "Error: closing $name instead of $currentTag\n";
	$currentTag = "";
} 

function characterData($parser, $data) 
{ 
	global $currentTag, $rResult; 
	$lineNb = sprintf('%06d', xml_get_current_line_number($parser));
	switch ($currentTag) { 
		case "LINK": 
			$rResult[]= $lineNb."<a href=\"$data\">$data</a>\n"; 
			break; 
		case "TITLE": 
			$rResult[]= $lineNb."title : $data\n"; 
			break; 
		default: 
			$rResult[]= $lineNb.$data."\n"; 
	} 
} 

echo "\n\n";
$xmlParser = xml_parser_create(); 

$targetEncoding = xml_parser_get_option($xmlParser, 
									  XML_OPTION_TARGET_ENCODING); 

xml_parser_set_option($xmlParser, XML_OPTION_CASE_FOLDING, true); 
	
xml_parser_set_option($xmlParser, XML_OPTION_SKIP_TAGSTART, 0); 

xml_set_element_handler($xmlParser, "startElement", "endElement"); 
xml_set_character_data_handler($xmlParser, "characterData");

$lines = explode("\n", (string)$tidy);
foreach($lines as $nb => $line)
{
	$rResult[]= $nb.$line."\n";
	xml_parse($xmlParser, $line."\n", false);
}
xml_parse($xmlParser, '', true);
xml_parser_free($xmlParser);

echo implode("\n", $rResult);
